package com.pruebadekra.usersmanager.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.fluttercode.datafactory.impl.DataFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pruebadekra.usersmanager.entities.User;

/**
 * Class to create users
 * @author Manuel
 *
 */
@Component
public class UserFaker {

	private DataFactory df = new DataFactory();
	
	@Autowired
	private CustomPasswordEncoder encoder;
	
	private User createUser(int id)
	{
		User user = new User();
		
		String firstName = df.getFirstName();
		String lastName = df.getLastName();
		
		user.setId(new Long(id));
		user.setUsername(getUsername(firstName, lastName));
		user.setFirstname(firstName);
		user.setLastname(lastName);
		user.setEmail(df.getEmailAddress());
		user.setPassword(encoder.encode(df.getRandomChars(8)));
		user.setAge(getRandomAge());
		
		return user;
	}
	
	private int getRandomAge() {
		int min = 18;
		int max = 100;
		
		Random r = new Random();
		
		return r.nextInt(max - min) + min;
	}
	
	private String getUsername(String firstName, String lastName) {
		return firstName.substring(0, 1).concat(lastName);
	}
	
	public List<User> createUsers(int numberOfUsers)
	{
		List<User> userList = new ArrayList<>();
		
		for (int i=0; i < numberOfUsers; i++) {
			userList.add(createUser(i+1));
		}
		
		return userList;
	}
	
}
