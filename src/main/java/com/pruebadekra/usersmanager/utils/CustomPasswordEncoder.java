package com.pruebadekra.usersmanager.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Class to encode de the user password
 * @author Manuel
 *
 */
@Component
public class CustomPasswordEncoder {
	
	private static final Logger logger = LoggerFactory.getLogger(CustomPasswordEncoder.class);

	private MessageDigest md;
	
	public CustomPasswordEncoder()
	{
		try {
            // Create MessageDigest instance for MD5
            md = MessageDigest.getInstance("MD5");
           
        }
        catch (NoSuchAlgorithmException e)
        {
        	logger.error("Algorith wrong");
            e.printStackTrace();
        }
	}
	
	public String encode(String password)
	{
		 //Add password bytes to digest
        md.update(password.getBytes());
        //Get the hash's bytes
        byte[] bytes = md.digest();
        //This bytes[] has bytes in decimal format;
        //Convert it to hexadecimal format
        StringBuilder sb = new StringBuilder();
        for(int i=0; i< bytes.length ;i++)
        {
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        //Get complete hashed password in hex format
        return sb.toString();
	}
}
