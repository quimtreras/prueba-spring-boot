package com.pruebadekra.usersmanager.utils.pagination;

import java.util.List;

public class Paginator<T> {

	private int currentPage;

	private int numberOfRecordPerPage;

	private int count;

	private int recordsInCurrentPage;

	private List<T> data;

	public int getPages() {
		return new Double(Math.ceil(count * 1f / numberOfRecordPerPage * 1f)).intValue();
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public boolean isLastPage() {
		return currentPage == getPages();
	}

	public int getNumberOfRecordPerPage() {
		return numberOfRecordPerPage;
	}

	public void setNumberOfRecordPerPage(int numberOfRecordPerPage) {
		this.numberOfRecordPerPage = numberOfRecordPerPage;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getRecordsInCurrentPage() {
		return recordsInCurrentPage;
	}

	public void setRecordsInCurrentPage(int recordsInCurrentPage) {
		this.recordsInCurrentPage = recordsInCurrentPage;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	public List<T> getData() {
		return data;
	}

}
