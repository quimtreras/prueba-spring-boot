package com.pruebadekra.usersmanager.repository;

import java.util.List;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.pruebadekra.usersmanager.entities.User;
import com.pruebadekra.usersmanager.utils.UserFaker;

/**
 * Class simulate User respository
 * @author Manuel
 *
 */
@Repository
public class UserRepository implements InitializingBean {
	
	@Autowired
	private UserFaker faker;

	/**
	 * User List is in memory while userRepository instance is in
	 * the Spring Container
	 */
	private List<User> userList;

	/**
	 * Method to get the User List
	 * @return
	 */
	public List<User> getUserList() {
		return userList;
	}

	/**
	 * When UserFaker is created, execute method to get a User List
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		userList = faker.createUsers(101);
		
	}
	
	 
}
