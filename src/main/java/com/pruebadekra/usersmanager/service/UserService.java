package com.pruebadekra.usersmanager.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.pruebadekra.usersmanager.entities.User;
import com.pruebadekra.usersmanager.utils.pagination.Paginator;

/**
 * User Service Interface
 * @author Manuel
 *
 */
@Service
public interface UserService {

	Optional<User> findUserById(Long id);
	
	List<User> getActiveUserList();
	
	Paginator<User> getPaginatedUserList(int page, int numberOfRecords);
	
	User save(User user);
	
	void delete(Long id); 
}
