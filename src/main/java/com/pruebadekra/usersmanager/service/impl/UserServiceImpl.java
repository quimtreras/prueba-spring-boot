package com.pruebadekra.usersmanager.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pruebadekra.usersmanager.entities.User;
import com.pruebadekra.usersmanager.repository.UserRepository;
import com.pruebadekra.usersmanager.service.UserService;
import com.pruebadekra.usersmanager.utils.pagination.Paginator;

/**
 * User Service Impl
 * @author Manuel
 *
 */
@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	private static final Predicate<User> FILTER_USERS_BY_ACTIVE = _user -> _user.getActive();

	@Override
	public Optional<User> findUserById(Long id) {
		return getActiveUserList().stream()
								  .filter(_user -> _user.getId().equals(id))
								  .findFirst();
	}

	@Override
	public List<User> getActiveUserList() {
		return userRepository.getUserList()
							 .stream()
							 .filter(FILTER_USERS_BY_ACTIVE)
							 .collect(Collectors.toList());
	}

	@Override
	public User save(User user) {
		Long userID = getUserListAutoincrementID();

		user.setId(userID);

		userRepository.getUserList().add(user);
		
		return user;
	}

	@Override
	public void delete(Long id) {
		Optional<User> currentUserOpt = findUserById(id);
		
		if (currentUserOpt.isPresent())
		{
			currentUserOpt.get()
						  .setActive(Boolean.FALSE);
		}
	}

	/**
	 * Autoincrement Method to get the next Id User value
	 * @return nextId for User
	 */
	private Long getUserListAutoincrementID() {
		Long maxID = userRepository.getUserList()
								   .stream().mapToLong(_user -> _user.getId())
								   .max()
								   .orElse(0l);
		return ++maxID;
	}

	@Override
	public Paginator<User> getPaginatedUserList(int page, int numberOfRecords) {
		Paginator<User> paginator = new Paginator<>();
				
		List<User> activeUserList = getActiveUserList();
		
		paginator.setCurrentPage(page);
		paginator.setNumberOfRecordPerPage(numberOfRecords);
		paginator.setCount(activeUserList.size());
		
		if (activeUserList.size() <= numberOfRecords) {
			paginator.setData(activeUserList);
			paginator.setRecordsInCurrentPage(activeUserList.size());
			return paginator;
		}
		
		activeUserList = activeUserList.stream()
							 .skip(page * numberOfRecords)
							 .limit(numberOfRecords)
							 .collect(Collectors.toList());
		paginator.setRecordsInCurrentPage(activeUserList.size());
		paginator.setData(activeUserList);
		
		return paginator;
	}

}
