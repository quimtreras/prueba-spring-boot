package com.pruebadekra.usersmanager.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pruebadekra.usersmanager.entities.User;
import com.pruebadekra.usersmanager.entities.groupvalidators.UserPostValidator;
import com.pruebadekra.usersmanager.entities.groupvalidators.UserPutValidator;
import com.pruebadekra.usersmanager.service.UserService;
import com.pruebadekra.usersmanager.utils.CustomPasswordEncoder;
import com.pruebadekra.usersmanager.utils.pagination.Paginator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/users")
@Api(tags = "users")
public class UserController {

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	private static final String USER_NOT_FOUND = "User with id %s is not present";
	
	private static final String DELETE_USER = "The user with id %s has been deleted";

	@Autowired
	private UserService userService;

	/**
	 * Because we are not using Spring security module,
	 * we need a way to encode the passwords
	 */
	@Autowired
	private CustomPasswordEncoder encoder;

	/**
	 * Get a User Active User List
	 * 
	 * @return
	 */
	@GetMapping
	@ApiOperation(value = "list", notes = "Service to show a users lists")
	public List<User> getAllUsers() {
		
		logger.info("Method getAllUsers");

		return userService.getActiveUserList();
	}
	
	@GetMapping("paginated")
	@ApiOperation(value = "list", notes = "Service to get a users lists paginated")
	public Paginator<User> getAllUsers(@RequestParam int page, @RequestParam int numberOfRecords) {
		
		logger.info("Method getAllUsersPaginated");

		return userService.getPaginatedUserList(page, numberOfRecords);
	}

	/**
	 * Get User Details
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping("{id}")
	@ApiOperation(value = "details", notes = "Service to show user details")
	public ResponseEntity<?> userDetails(@PathVariable Long id) {
		
		logger.info(String.format("Method userDetails -- params -- id: %s", id));

		try {
			User user = userService.findUserById(id)
					.orElseThrow(() -> new Exception(String.format(USER_NOT_FOUND, id)));

			return new ResponseEntity<User>(user, HttpStatus.OK);

		} catch (Exception ex) {

			return handlerError(ex);
		}
	}

	/**
	 * Method to create a new User
	 * 
	 * @param user
	 * @return
	 */
	@PostMapping
	@ApiOperation(value = "create", notes = "Service to create users")
	public ResponseEntity<?> create(@Validated(UserPostValidator.class) @RequestBody User user, BindingResult result) {
		
		logger.info("Method create -- params -- user: " + user.toString());

		if (result.hasErrors()) {
			return handleErrorValidations(result.getFieldErrors());
		} else {
			user.setPassword(encoder.encode(user.getPassword()));
			userService.save(user);

			return new ResponseEntity<User>(user, HttpStatus.CREATED);
		}

	}

	/**
	 * Method to update User No password, active or id fields are updated
	 * 
	 * @param id
	 * @param user
	 * @return
	 */
	@PutMapping("{id}")
	@ApiOperation(value = "update", notes = "Service to update an user")
	public ResponseEntity<?> update(@PathVariable Long id, @Validated(UserPutValidator.class) @RequestBody User user, BindingResult result) {
		
		logger.info(String.format("Method update -- params -- id: %s, user: %s ", id, user.toString()));

		if (result.hasErrors()) {
			return handleErrorValidations(result.getFieldErrors());
		} else {
			try {
				User currentUser = userService.findUserById(id)
						.orElseThrow(() -> new Exception(String.format(USER_NOT_FOUND, id)));

				currentUser.setUsername(user.getUsername());
				currentUser.setFirstname(user.getFirstname());
				currentUser.setLastname(user.getLastname());
				currentUser.setEmail(user.getEmail());
				currentUser.setAge(user.getAge());
				

				return new ResponseEntity<User>(currentUser, HttpStatus.OK);

			} catch (Exception ex) {

				return handlerError(ex);
			}
		}

	}

	/**
	 * Method to do delete (soft delete)
	 * 
	 * @param id
	 * @return
	 */
	@DeleteMapping("{id}")
	@ApiOperation(value = "delete", notes = "Service to delete users")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		
		logger.info(String.format("Method delete -- params -- id: %s", id));
		
		try {
			User currentUser = userService.findUserById(id)
					.orElseThrow(() -> new Exception(String.format(USER_NOT_FOUND, id)));

			userService.delete(currentUser.getId());

			Map<String, String> messages = new HashMap<String, String>();

			messages.put("message", String.format(DELETE_USER, id));

			return new ResponseEntity<Map<String, String>>(messages, HttpStatus.OK);

		} catch (Exception ex) {

			return handlerError(ex);
		}
	}

	/**
	 * Handle common errors for Get, Put and Delete Method when a user is not found
	 * 
	 * @param ex
	 * @return
	 */
	private ResponseEntity<?> handlerError(Exception ex) {
		
		logger.error(ex.getCause().getMessage());
		
		Map<String, String> messages = new HashMap<>();

		messages.put("error", ex.getMessage());

		return new ResponseEntity<Map<String, String>>(messages, HttpStatus.NOT_FOUND);
	}

	/**
	 * Method to handle error validations from request
	 * @param fieldErrorList
	 * @return
	 */
	private ResponseEntity<?> handleErrorValidations(List<FieldError> fieldErrorList) {
		
		
		Map<String, String> errors = new HashMap<String, String>();

		fieldErrorList.forEach(fieldError -> {
			logger.info(fieldError.getDefaultMessage());
			errors.put(fieldError.getField(), fieldError.getDefaultMessage());
		});

		return new ResponseEntity<Map<String, String>>(errors, HttpStatus.BAD_REQUEST);
	}
}
