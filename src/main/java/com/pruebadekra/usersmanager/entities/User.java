package com.pruebadekra.usersmanager.entities;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.pruebadekra.usersmanager.entities.groupvalidators.UserPostValidator;
import com.pruebadekra.usersmanager.entities.groupvalidators.UserPutValidator;

/**
 * Entity User
 * @author Manuel
 *
 */
public class User
{
	/**
	 * User ID
	 */
	private Long id;
	
	@NotNull(groups = { UserPostValidator.class, UserPutValidator.class })
	@Size(min = 4, max = 10, groups = { UserPostValidator.class, UserPutValidator.class })
	private String username;
	
	@NotNull(groups = { UserPostValidator.class, UserPutValidator.class })
	@Size(min = 4, max = 10, groups = { UserPostValidator.class, UserPutValidator.class })
	private String firstname;
	
	@NotNull(groups = { UserPostValidator.class, UserPutValidator.class })
	@Size(min = 4, max = 70, groups = { UserPostValidator.class, UserPutValidator.class })
	private String lastname;
	
	@NotNull( groups = { UserPostValidator.class, UserPutValidator.class })
	@Email
	@Size(max = 30, groups = { UserPostValidator.class, UserPutValidator.class })
	private String email;
	
	@NotNull( groups = { UserPostValidator.class })
	@Size(min = 8, max = 20, groups = { UserPostValidator.class, })
	private String password;
	
	@NotNull(groups = { UserPostValidator.class, UserPutValidator.class })
	@Max(100)
	@Min(18)	
	private Integer age;
	
	private Boolean active = Boolean.TRUE;
	
	public User() {
		
	}

	public User(Long id, String username, String firstName, String lastname, String email, String password, Integer age
			) {
		super();
		this.id = id;
		this.username = username;
		this.firstname = firstName;
		this.lastname = lastname;
		this.email = email;
		this.password = password;
		this.age = age;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstName) {
		this.firstname = firstName;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastName) {
		this.lastname = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", firstName=" + firstname + ", lastName=" + lastname
				+ ", email=" + email + ", age=" + age + ", active=" + active + "]";
	}

}
